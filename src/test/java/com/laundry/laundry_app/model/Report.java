package com.laundry.laundry_app.model;

import lombok.Data;

@Data
public class Report {
    private Long customerId;
    private String customerName;
    private Double totalAmount;
}
