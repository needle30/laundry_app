package com.laundry.laundry_app.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.laundry.laundry_app.model.Report;
import com.laundry.laundry_app.repository.CustomerRepository;
import com.laundry.laundry_app.repository.OrderRepository;

@Service
public class ReportService {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderRepository orderRepository;

    public List<Report> generateReport() {
        return customerRepository.findAll().stream().map(customer -> {
            Report report = new Report();
            report.setCustomerId(customer.getId());
            report.setCustomerName(customer.getName());
            report.setTotalAmount(orderRepository.findAll().stream()
                    .filter(order -> order.getCustomerId().equals(customer.getId()))
                    .mapToDouble(order -> order.getTotalPrice()).sum());
            return report;
        }).collect(Collectors.toList());
    }

}
