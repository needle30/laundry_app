package com.laundry.laundry_app.model;

import jakarta.persistence.Entity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
@Data
@Entity

public class Report {
    private Long customerId;
    private String customerName;
    private Double totalAmount;
}