package com.laundry.laundry_app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.laundry.laundry_app.model.Customer;

public interface CustomerRepository extends JpaRepository< Customer, Long> {
    
}
