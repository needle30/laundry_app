package com.laundry.laundry_app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.laundry.laundry_app.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
    
}
