package com.laundry.laundry_app.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.laundry.laundry_app.model.Report;
import com.laundry.laundry_app.service.ReportService;

import java.util.List;

@RestController
@RequestMapping("/api/reports")
public class ReportController {
    @Autowired
    private ReportService reportService;

    @GetMapping
    public List<Report> generateReport() {
        return reportService.generateReport();
    }
}
